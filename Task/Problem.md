# Problem

**Problem 1** 

Abraham

Surfboard Abraham

Abraham

Surfboard Abraham

Abraham

Surfboard Abraham

**Problem 2**

Let A be 1001001001001

Let B be 12345567891

Let C be 559922932941

Removing last digit

A becomes 100100100100

B becomes 1,234,556,789

C becomes 55,992,293,294

Multiplying the digits by 2

200,200,200,200

2468101012141618

10101818441864184

Adding up the digits 

A becomes 8

B becomes 1

C becomes 6

A is invalid

B is valid 

C is invalid

Problem 3

Let the set of numbers be (1,2)

- Maximum is set to 0 as per step 2
- Let’s take first number 1 as input
- Comparing the taken number with maximum set value
- 1 is greater than 0, so max is set the greater number ie 1
- Again we jump to step 3, the next number from the set is taken as input ie 2
- Comparing the taken number with maximum set value
- 2 is greater than 0, so max is set the greater number ie 2
- All numbers are exhausted
- Now the max set value becomes the largest number
- As per step 6, 2 is printed

Problem 4 (Tower of  Hanoi) 

Let us consider the three needles as A B and C

Constrains: 

Move disk 1 from rod A to rod B

Move disk 2 from rod A to rod C

Move disk 1 from rod B to rod C

Move disk 3 from rod A to rod B

Move disk 1 from rod C to rod A

Move disk 2 from rod C to rod B

Move disk 1 from rod A to rod B

Move disk 4 from rod A to rod C

Move disk 1 from rod B to rod C

Move disk 2 from rod B to rod A

Move disk 1 from rod C to rod A

Move disk 3 from rod B to rod C

Move disk 1 from rod A to rod B

Move disk 2 from rod A to rod C

Move disk 1 from rod B to rod C

Problem 5

- Let the number A be abcd
- Let the number B be efgh
- Multiplication is adding (abcd),  (efgh) times
- Add A, B times

Problem 6

Check whether the number a, b is divisible by any other number other than the a,b respectively and one. If yes, continue to step 1. If no, stop.

1. If both a and b are 0, gcd is zero gcd(0, 0) = 0.
2. gcd(a, 0) = a and gcd(0, b) = b because everything divides 0.
3. If a and b are both even, gcd(a, b) = 2*gcd(a/2, b/2) because 2 is a common divisor.
4. If a is even and b is odd, gcd(a, b) = gcd(a/2, b). Similarly, if a is odd and b is even, then gcd(a, b) = gcd(a, b/2). It is because 2 is not a common divisor.
5. If both a and b are odd, then gcd(a, b) = gcd(|a-b|/2, b). Note that difference of two odd numbers is even
6. Repeat steps 3–5 until a = b, or until a = 0. In either case, the GCD is power(2, k) * b, where power(2, k) is 2 raise to the power of k and k is the number of common factors of 2 found in step 2.

Problem 7

The given message is ;: :;;; ;:; ;: ;;;; ;: ::

Let the value of a be ;

Let the value of b be :

Problem 8

Problem 9

Check whether the monster is having teeth on the right side

Check whether the monster is wearing a white tie

If both steps are satisfied, then it is the real monster