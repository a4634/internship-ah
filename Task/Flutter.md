# Flutter

Flutter is a opensource UI development kit or frame work. It was developed by Google. It’s use to develop fast native apps like iOS, Android, Windows, Mac, Linux. Single code base to develop native applications. First Flutter was called Sky. It is based on C, C++ and Dart programming language. 

Installing Flutter 

- Go to [https://flutter.dev/](https://flutter.dev/)
- Click on Get Started
- Select the operating system on which you are installing Flutter
- Download the setup file
- Run the setup file by enter the command tar -xvf name of the package
- Download Android Studio setup
- Install Android Studio. Run the setup file by enter the command tar -xvf name of the package
- Open terminal window
- To open flutter, go to bin folder in flutter main folder
- sudo ln -a give the folder path
- Repeat the same for Android Studio
- Install snapd, this is the fastest way to install flutter
- Run flutter doctor in terminal

flutter doctor

flutter doctor will check for dependencies needed for flutter application. It will list the major dependencies  with the availability status. Major dependencies of flutter are

- Android toolchain
- Chrome
- Android Studio
- Connected devices
- HTTP Host availability

Jawa jdk is one of the requirement to run flutter. So Jawa sdk setup file is downloaded and installed.

- Search for 'edit the system environment variables' on search menu
- When system properties dialog open you find the menu called Environment Variables almost at the bottom Click on it
- Add the path where the java is installed
- Android license - flutter doctor — android-licenses
- Give yes for the questions