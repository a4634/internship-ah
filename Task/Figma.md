# Figma

One of the top websites for UI designers is Figma. Figma is used for mobile web designing, desktop web designing also used for other designing works. Figma is a professional website for UI design. Features of Figma 

- Design - UI screens can be designed using Figma
- Prototyping - Screen interaction  can be made clear using Figma
- Collaboration- The working file can be shared and collaborated with 2 team members in a easier manner for free version

Figma has both free version and premium. In free version two projects can be done at free of cost. 

Live editing can be done using Figma. It has a very simple and easier user interface. Figma desktop application is provided for mac and windows and it is not available for other OS.

Figma is a simple light weighted application. 

Frame tool gives different sizes of frame. For example, tablet, mobile, watches etc. Frame is the art board. In Figma art board is considered as shape. Frames can be changed in layer alone. User can lock the layer and also enable and disable visibility. Some of the shortcuts in Figma are

Hold shift for aspect rescaling

Ctrl+ zoom in

Ctrl- zoom out

Space + left mouse 

Move shortcut V

Alt +drag creates smart guides

Press shift and you can select multiple art boards

**Material Design**

Material Design is a design language developed by Google in 2014. Material Design uses more grid-based layouts, responsive animations and transitions, padding, and depth effects such as lighting and shadows. It is the design foundation for Google products and other brands too. 

The whole thing is based on real world physics. Theming is a major update to material design.